<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::get('verify-account/{token}', 'AuthController@verifyUser');
    Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([

    'middleware' => 'api',

], function ($router) {

    Route::get('users', 'UserController@index');
    Route::get('user/get-by-token', 'UserController@getUserByToken');
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::get('users/get-by-email/{email}', 'UserController@getByEmail');
    Route::post('user/register', 'UserController@store');
    Route::get('closed', 'DataController@closed');

    // Roles
    Route::get('role',                          'RoleController@index');
    Route::get('role/{id}',                     'RoleController@show');
    Route::post('role',                         'RoleController@store');
    Route::put('role/{id}',                     'RoleController@update');
    Route::delete('role/{id}',                  'RoleController@destroy');
    Route::post('role/give-permission',     	'RoleController@givePermission');
    Route::post('role/revoke-permission',   	'RoleController@revokePermission');

    // Permissions
    Route::get('permission',                    'Permission@index');
    Route::get('permission/{id}',               'Permission@show');
    Route::post('permission',                   'Permission@store');
    Route::put('permission/{id}',               'Permission@update');
    Route::delete('permission/{id}',            'Permission@destroy');
    Route::post('permission/assign-role',   	'Permission@assignRole');
    Route::post('permission/remove-role',   	'Permission@removeRole');

});
