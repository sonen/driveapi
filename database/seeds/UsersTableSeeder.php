<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();
        $users = [
            [
                'first_name' => 'Admin',
                'last_name'  => 'System',
                'email'      => 'admin@drivelicense.co.ug',
                'password'   => '12345678'
            ],
            [
                'first_name'  => 'John',
                'last_name'   => 'Doe',
                'email'       => 'jdoe@drivelicense.co.ug',
                'password'    => '12345678'
            ],
            [
                'first_name'  => 'Jane',
                'last_name'   => 'Doe',
                'email'       => 'janedoe@drivelicense.co.ug',
                'password'    => 'password'
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }

        DB::table('model_has_roles')->insert(
            [
                'role_id' => 1,
                'model_type' => 'App\User',
                'model_id'  => 1
            ],
            [
                'role_id' => 2,
                'model_type' => 'App\User',
                'model_id'  => 2
            ]
        );

        Model::reguard();
    }
}
