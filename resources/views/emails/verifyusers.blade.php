@component('mail::message')
# Hello
{{$user->email}},

Welcome to the drive license portal, please click the button below to activate your account.

<a href="{{url('api/auth/verify-account', $user->verifyUser->token)}}" class="btn btn-primary">Verify Email</a>

@component('mail::button', ['url' => 'verify-account/{{$user->verifyuser->token}}'])
Activate my account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
