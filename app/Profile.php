<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['date_of_birth', 'gender', 'ID_number', 'profile_photo', 'user_id'];

    public function user() {
        return $this->brlongsTo(User::class, 'user_id');
    }
}
