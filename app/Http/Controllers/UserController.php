<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Spatie\Permission\Models\Role;
use App\Verifyuser;
use App\Mail\verifyUsers;
use Mail;

class UserController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'store', 'getByEmail']]);

        $this->middleware('role:super-admin', [
                'only' => [
                    'destroy',
                    'assignRole',
                    'removeRole',
                    'givePermissionTo',
                    'revokePermissionTo'
                ]]);
    }

    public function index() {
        $users = User::with(['profile', 'roles'])->orderByDesc('created_at')->get();

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|unique:users|max:60',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return $this->sendResponse($validator->errors(), 'Validation Failed');
        }

        $user = User::create($request->all());

        $user->assignRole('Motorist');

        $verifyUser = Verifyuser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        Mail::to($user->email)->send(new verifyUsers($user));

        return $this->sendResponse($user->toArray(), 'User registered successfullly');
    }

    public function show($id) {

    }

    public function update(Request $request) {

    }

    public function destory($id) {

    }

    public function getUserByToken() {

        $token = JWTAuth::getToken();

        $user = JWTAuth::toUser($token);

        return $this->sendResponse($user->toArray(), 'User retrieved succesfully');
    }

    public function getByEmail($email) {
        $user = User::with(['profile', 'roles'])->where('email', $email)->get();

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }




}
