<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Validator;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => []]);
    }

    public function index()
    {
        $permissions = Permission::all();

        return $this->sendResponse($permissions->toArray(), 'permissions retrieved successfully.');
    }


    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'name' => 'required|unique:permissions'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $permission = new Permission;
        $permission->name = str_slug($input['name']);
        $permission->guard_name = 'web';
        $permission->save();


        return $this->sendResponse($permission->toArray(), 'Permission created successfully.');
    }


    public function show($id)
    {
        $permission = Permission::findOrFail($id);

        if (is_null($permission)) {
            return $this->sendError('Permission not found.');
        }

        return $this->sendResponse($permission->toArray(), 'Permission retrieved successfully.');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'name' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $permission = Permission::findOrFail($id);
        $permission->name = $input['name'];
        $permission->save();


        return $this->sendResponse($permission->toArray(), 'Permission updated successfully.');
    }

    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        $permission->delete();

        return $this->sendResponse($permission->toArray(), 'Permission deleted successfully.');
    }

    public function assignRole(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'permission_id' => 'required',
            'roles' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $permission = Permission::find($input['permission_id']);
        $permission->syncRoles($input['roles']);


        return $this->sendResponse($role->toArray(), 'Permission assigned successfully.');
    }

    public function removeRole(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'permission_id' => 'required',
            'role_id' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $permission = Role::find($input['permission_id']);
        $permission->removeRole($input['role_id']);


        return $this->sendResponse($role->toArray(), 'Role removed successfully.');
    }
}
