<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Validator;

class RoleController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => []]);
    }

    public function index()
    {
        $roles = Role::with('permissions')->get();


        return $this->sendResponse($roles->toArray(), 'roles retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'name' => 'required|unique:roles'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $role = new Role;
        $role->name = str_slug($input['name']);
        $role->guard_name = 'web';
        $role->save();


        return $this->sendResponse($role->toArray(), 'Role created successfully.');
    }

    public function show($id)
    {
        $role = Role::find($id);


        if (is_null($role)) {
            return $this->sendError('Role not found.');
        }


        return $this->sendResponse($role->toArray(), 'Role retrieved successfully.');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'name' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $role = Role::findOrFail($id);
        $role->name = str_slug($input['name']);
        $role->save();


        return $this->sendResponse($role->toArray(), 'Role updated successfully.');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $role->delete();


        return $this->sendResponse($role->toArray(), 'Role deleted successfully.');
    }

    public function givePermission(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'permissions' => 'required',
            'role_id' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $role = Role::find($input['role_id']);
        $role->syncPermissions($input['permissions']);


        return $this->sendResponse($role->toArray(), 'Role given permissions successfully.');
    }

    public function revokePermission(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'permission_id' => 'required',
            'role_id' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $role = Role::find($input['role_id']);
        $role->revokePermissionTo($input['permission_id']);


        return $this->sendResponse($role->toArray(), 'Role permission revoked successfully.');
    }
}
